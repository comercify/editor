//webpack --display-reasons
//Remove jquery https://github.com/webpack/webpack/issues/1275
var webpack = require('webpack');
var path = require('path')
var pkg = require('./package.json');
var env = process.env.WEBPACK_ENV;
var name = 'grapes';
var plugins = [];

if(env !== 'dev'){
  plugins = [
    new webpack.optimize.UglifyJsPlugin({
      //sourceMap: true,
      minimize: true,
      compressor: {warnings: false},
    }),
    new webpack.BannerPlugin(pkg.name + ' - ' + pkg.version),
    //v2 new webpack.BannerPlugin({banner: 'Banner v2'});
  ]
}

plugins.push(new webpack.ProvidePlugin({_: 'underscore'}));

module.exports = {
  entry: './myEditor/main.js',
  output: {
    path: path.resolve(__dirname, './dist2'),
    publicPath: '/',
    filename: 'build.js'
  },
  // output: {
  //     filename: './dist/' + name + '.min.js',
  //     library: 'grapesjs',
  //     libraryTarget: 'umd',
  // },
  externals: {jquery: 'jQuery'},
  plugins: plugins,
  module: {
    loaders: [{
        test: /\.js$/,
        loader: 'babel-loader',
        include: /src/,
        exclude: /node_modules/,
        query: {presets: ['es2015']}
    }],
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
          }
          // other vue-loader options go here
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]'
        }
      }
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
    },
    modules: ['src', 'node_modules','myEditor'],
  },
}
